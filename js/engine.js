$(function() {

	//	Появление главного меню 
	$('.header-menu__btn').click(function() {
		$('.main-menu__wrapper').toggleClass("active");
	})

	//	Появление фильтра
	$('.header-btn__filter').click(function() {
		$('.header-filter').toggleClass("active");
	})

	//	Появление фильтра истории
	$('.header-btn__story').click(function() {
		$('.header-story').toggleClass("active");
	})

	$('select').styler();
})